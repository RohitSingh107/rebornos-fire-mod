# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import json
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Unnecessary Packages")
try:
    import httplib
except:
    import http.client as httplib

# Create variables for both the current working directory and the location of the settings file
workingDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
gladeFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Glade', 'RebornRemove.glade'))
localeDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'TranslationFiles'))

# Create Handlers (Triggers) for each item
class Handler:

# Close the window
    def onDestroy5(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Save Program List
    def onEntryRemove(self, pkgtxt):
        global enteredText2
        enteredText2 = pkgtxt.get_text()
        print("Entered Text: ", enteredText2)

# Recover From
    def onRemove(self, button):
        print("Entered Text: ", enteredText2)
        Notify.Notification.new("Removing...").show()
        os.system('pkexec pacman -Rdd '  + enteredText2 + ' --noconfirm')
        Gtk.main_quit()

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(gladeFile)
builder.connect_signals(Handler())

# Set Labels
with open(localeDirectory + '/translations_' + os.getenv('LANG').split('_')[0] + '.json') as json_file:
    locale = json.load(json_file)
    builder.get_object("RemovePackageButton").set_label(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["RemovePackageDialog"]["RemoveButton"])
    builder.get_object("InsertPackageToRemove").set_placeholder_text(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["RemovePackageDialog"]["RemoveInsert"])

window1 = builder.get_object("Reborn5")
window1.show_all()

Gtk.main()

Notify.uninit()
