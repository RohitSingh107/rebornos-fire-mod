# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("RebornOS Customization")
try:
    import httplib
except:
    import http.client as httplib

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. The following operations will not work").show()

# Create variables for both the current working directory and the location of the settings file
bashFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Bash', 'Maintenance.sh'))
workingDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
gladeFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Glade', 'RebornCustomization.glade'))

# Create Handlers (Triggers) for each item
class Handler:

# Close the window
    def onDestroy4(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Store directory location
    def onDirectoryChosen(self, menuitem):
        global filechosen
        filechosen = menuitem.get_file().get_path()
        print("selected folder: ", filechosen)
        print()

# Store Repository URL
    def gitlabURL(self, pkgtxt):
        global enteredURL
        enteredURL = pkgtxt.get_text()

# Store Gitlab username
    def gitlabUsername(self, pkgtxt):
        global enteredUsername
        enteredUsername = pkgtxt.get_text()

# Store Gitlab password
    def gitlabPassword(self, pkgtxt):
        global enteredPassword
        enteredPassword = pkgtxt.get_text()

# Button actions
    def buttonClicked(self, button):
        os.system('bash ' + bashFile + ' Contribution ' + filechosen + ' ' + enteredURL + ' ' + enteredUsername + ' ' + enteredPassword + ' ' + workingDirectory)

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(gladeFile)
builder.connect_signals(Handler())
window5 = builder.get_object("RebornCustomization")
window5.show_all()

Gtk.main()

Notify.uninit()
