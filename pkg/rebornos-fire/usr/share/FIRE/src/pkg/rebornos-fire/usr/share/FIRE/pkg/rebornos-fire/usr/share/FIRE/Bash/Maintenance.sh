#!/bin/bash
#set -x
directory=/usr/share/RUM
lightdmGreeters=ligthdm.txt
sddmGreeters=sddm.txt

function LightDM() {
    line_number=grep -n "greeter-session" /etc/lightdm/lightdm.conf | grep -v "Session to load for greeter" | cut -f1 -d ":"
    pkexec sed -i "${line_number}s|.*|$1|" /etc/lightdm/lightdm.conf
}

function SDDM() {
      if [[ $(grep -n "Current=" /etc/sddm.conf | cut -f1 -d ":" | wc -l) -le 0 ]]; then
            if [[ $(grep -n "[Theme]" /etc/sddm.conf | wc -l) -le 0 ]]; then
                  echo "[Theme]" >> /etc/sddm.conf
            fi
            echo "Current=$1" >> /etc/sddm.conf
      else
            line_number=$(grep -n "Current=" /etc/sddm.conf | cut -f1 -d ":")
            sed -i "${line_number}s|.*|Current=$1|" /etc/sddm.conf
      fi
}

function TestLightdm() {
      if [ ! -f /usr/bin/Xephyr ] || [ ! -f /usr/bin/lightdm-gtk-greeter ] || [ ! -f /usr/bin/lightdm-webkit2-greeter ]; then

            pkexec pacman -S xorg-server-xephyr lightdm-gtk-greeter lightdm-webkit2-greeter --needed --noconfirm
      fi
      originalText=$(grep "greeter-session" /etc/lightdm/lightdm.conf | grep -v "Session to load for greeter")
      currentTheme=$(grep -n "greeter-session" /etc/lightdm/lightdm.conf | grep -v "Session to load for greeter" | sed "s|^.*=||")
      line_number=$(grep -n "greeter-session" /etc/lightdm/lightdm.conf | grep -v "Session to load for greeter" | cut -f1 -d ":")
      pkexec sed -i "${line_number}s|.*|greeter-session=$1|" /etc/lightdm/lightdm.conf
      lightdm --test-mode --debug
      pkexec sed -i "${line_number}s|.*|$originalText|" /etc/lightdm/lightdm.conf
}

function SDDMdefault() {
      if [[ $(pacman -Qs sddm | grep "local/sddm " | awk '{print $1}' | wc -l) -eq 0 ]]; then
            yes | sudo pacman -S sddm
      fi
      systemctl enable sddm.service --force
      systemctl start sddm.service
      echo "Done!"
}

function LightDMdefault() {
      if [[ $(pacman -Qs lightdm | grep "local/lightdm " | awk '{print $1}' | wc -l) -eq 0 ]]; then
            yes | pacman -S lightdm
      fi
      systemctl enable lightdm.service --force
      systemctl start lightdm.service
}

function Grub()
{
      os-prober
      grub-mkconfig -o /boot/grub/grub.cfg
}

function InstallTools() {
      # Store terminal choice from settings
      terminal=$2

      applicationRun=$1
      applicationInstall=$1
      # If the application name and the command used to start it are different
      if [ "$1" == "pyakm-manager" ]; then
            applicationInstall="pyakm"
      elif [ "$1" == "pamac-manager" ]; then
            applicationInstall="pamac"
      fi

      # If stacer was the application selected, run the below code
      if [ "$1" == "stacer" ]; then
            # Install Stacer if necessary
            if [[ $(which stacer | wc -l) -eq 0 ]]; then
                  eval $terminal "yay -S $1 --noconfirm"
            fi
            # Run Stacer
            $1 &

      # If pace was the application selected, run the below code
      elif [ "$1" == "pace" ]; then
            # Install Pace if necessary
            if [[ $(which pace | wc -l) -eq 0 ]]; then
                  eval $terminal "yay -S $1 --noconfirm"
            fi
            # Run Pace
            $1 &

      # If pamac was the application selected, run the below code
      elif [ "$applicationInstall" == "pamac" ]; then
                  # Install Pamac if necessary
                  if [[ $(which pamac | wc -l) -eq 0 ]]; then
                        $terminal "yay -S $applicationInstall --noconfirm"
                  fi
                  # Run Pamac
                  $applicationRun &

            # If pyakm was the application selected, run the below code
      elif [ "$applicationInstall" == "pyakm" ]; then
            # Install Pyakm if necessary
            if [[ $(which pyakm | wc -l) -eq 0 ]]; then
                  eval $terminal "yay -S $applicationInstall --noconfirm"
            fi
            # Run Pyakm
            $applicationRun &
      fi
}

function DesktopEnvironment() {
      # Detect the user-specified terminal
      terminal=$2

      if [ "$1" == "gnome" ]; then
            echo "gnome"
            # eval $terminal "sudo pacman -S gnome-common gnome-font-viewer gnome-keyring gnome-terminal qt5-styleplugins qt5ct gnome-online-accounts libgnome-keyring --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-gnome --noconfirm"
      elif [ "$1" == "apricity" ]; then
            echo "apricity"
            # Download and install apricity-theme from the main RebornOS repository
            curl -o rebornos.txt http://repo.rebornos.org/RebornOS/
            themeFile=$(grep "apricity-theme" rebornos.txt | cut -f2 -d "\"" | cut -d "\"" -f1 | grep -v "sig")
            # Install packages with only one pkexec dialog
            # eval $terminal "sudo pacman -U $themeFile --needed --noconfirm"
            eval xterm -e "sudo pacman -U $themeFile --noconfirm"
            rm -f rebornos.txt
      elif [ "$1" == "cinnamon" ]; then
            echo "cinnamon"
            # eval $terminal "sudo pacman -S $1 muffin nemo-fileroller nemo-share nemo-preview nemo-seahorse seahorse gnome-common gnome-font-viewer gnome-keyring gnome-terminal qt5-styleplugins qt5ct gnome-online-accounts libgnome-keyring --needed --noconfirm"
            # eval $terminal "sudo yay -S mint-themes mint-x-icons mint-y-icons mintlocale mintstick --noconfirm --needed"
            eval xterm -e "sudo pacman -S rebornos-cosmic-cinnamon --noconfirm"
      elif [ "$1" == "deepin" ]; then
            echo "deepin"
            # eval $terminal "sudo pacman -S redshift deepin deepin-extra noto-fonts noto-fonts-emoji unicode-character-database blueman --needed --noconfirm"
            # eval $terminal "sudo pacman -Rdd qt5ct --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-deepin --noconfirm"
      elif [ "$1" == "budgie" ]; then
            echo "budgie"
            # eval $terminal "sudo pacman -S bugdie-desktop budgie-extras gnome gnome-tweaks --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-budgie --noconfirm"
      elif [ "$1" == "enlightenment" ]; then
            echo "enlightenment"
            # eval $terminal "sudo pacman -S enlightenment connman qt5ct terminology xdg-user-dirs-gtk --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-enlightenment --noconfirm"
      elif [ "$1" == "i3" ]; then
            echo "i3"
            # eval $terminal "sudo pacman -S i3 dmenu file-roller network-manager-applet networkmanager-openvpn networkmanager-pptp nitrogen pcmanfm polkit-gnome xdg-user-dirs-gtk notify-osd python-pysmbc lxappearance-gtk3 lxinput-gtk3 awesome-terminal-fonts --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-i3 --noconfirm"
      elif [ "$1" == "plasma" ]; then
            echo "plasma"
            # eval $terminal "sudo pacman -S gnu-free-fonts spectacle kcolorchooser sshfs gwenview plasma kf5 kdebase --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-kde --noconfirm"
      elif [ "$1" == "lxqt" ]; then
            echo "lxqt"
            # eval $terminal "sudo pacman -S lxqt packagekit-qt5 xdg-user-dirs libstatgrab libsysstat network-manager-applet file-roller unace xscreensaver xautolock lxqt-build-tools --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-lxqt --noconfirm"
      elif [ "$1" == "mate" ]; then
            echo "mate"
            # eval $terminal "sudo pacman -S mate mate-extra gnome-keyring network-manager-applet networkmanager-applet xdg-user-dirs-gtk mate-tweak qt5ct"
            eval xterm -e "sudo pacman -S rebornos-cosmic-mate --noconfirm"
      elif [ "$1" == "openbox" ]; then
            echo "openbox"
            # eval $terminal "sudo pacman -S lxde file-roller arandr openbox-menu compton lxmenu-data lxappearance-obconf-gtk3 obkey oblogout networkmanager-openvpn networkmanager-pptp notify-osd nitrogen mousepad obconf parcellite polkit qt5ct scrot tint2 volumeicon udiskie xdg-user-dirs-gtk openbox-theme --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-openbox --noconfirm"
      elif [ "$1" == "xfce" ]; then
            echo "xfce"
            # eval $terminal "sudo pacman -S xfce4 xfce4-goodies file-roller gtk-engine-murrine network-manager-applet networkmanager-openvpn networkmanager-pptp polkit-gnome qt5ct xdg-user-dirs-gtk ristretto xfce4-whiskermenu-plugin gnome-font-viewer catfish libgit2-glib --needed --noconfirm"
            eval xterm -e "sudo pacman -S rebornos-cosmic-xfce --noconfirm"
      elif [ "$1" == "pantheon" ]; then
            echo "pantheon"
            # eval $terminal "yay -S pantheon-session-git gala-git wingpanel-indicator-datetime-git wingpanel-indicator-session-git wingpanel-git cerbere-git pantheon-applications-menu-git switchboard-git pantheon-dpms-helper-git pantheon-default-settings-git gnome-settings-daemon-elementary pantheon-print-git pantheon-polkit-agent-git --noconfirm"
            # pkexec pacman -S pantheon bzr --needed --noconfirm
            eval xterm -e "sudo pacman -S rebornos-cosmic-pantheon --noconfirm"
      fi
}

function Rollback() {
      # Declare variables
      day=$1
      month=$2
      year=$3
      # Detect the user-specified terminal
      terminal=$4

      # Create the correct URL variable for the mirrorlist file
      url1="Server = https://archive.archlinux.org/repos/$year/$month/$day/"
      url2='$repo/os/$arch'
      url=$url1$url2
      # Backup the up-to-date mirrorlist
      if [ $(grep -w '/etc/pacman.d/mirrorlist' -e 'Server = https://archive.archlinux.org' | wc -l) -eq 0 ]; then
            cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
      fi
      echo $url > /etc/pacman.d/mirrorlist
      eval $terminal "sudo pacman -Syyuu --noconfirm"
}

function ResetRollback()
{
      # Store terminal choice from settings
      terminal=$1
      if [ -f /etc/pacman.d/mirrorlist.bak ]; then
            pkexec mv -f /etc/pacman.d/mirrorlist.bak /etc/pacman.d/mirrorlist
      fi
      eval $terminal "sudo pacman -Syyuu --noconfirm"
}

function Downgrade()
{
      # Store terminal choice from settings
      eval $2 "pkexec downgrade $1"
}

function Contribution()
{
      # Rename variables for convenience
      directoryChosen=$1
      gitlabURL=$2
      gitlabUsername=$3
      gitlabPassword=$4
      expectDirectory=$5
      # cd into git directory
      cd $directoryChosen
      # If the directory is not a git repository yet, make it one
      if [ ! -d $directoryChosen/.git ]; then
            # Initiate the .git repository
            git init
            # Add some necessary configs
            git config --local user.email "$gitlabUsername@example.com"
            git config --local user.name "$gitlabUsername"
            # Add origin URL
            git remote add origin $gitlabURL
      fi
      # Add new content
      git add .
      # Commit it
      git commit -m "Updating..."
      # Pass username and password to Expect.exp script here
      expect $expectDirectory/Bash/Expect.exp $gitlabUsername $gitlabPassword
}

function InstallAnbox()
{
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})

      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      if [[ $(snap list | grep "anbox" | wc -l) -gt 0 ]]; then

            # Notify the user that this will most likely take a few minutes
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Please be patient..." "This could take a few minutes." -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"
            # Uninstall the Anbox snap package
            sudo snap remove anbox
            # Remove SystemD service
            sudo systemctl disable anbox-reborn.service
            sudo systemctl stop anbox-reborn.service
            sudo rm -f /etc/systemd/system/anbox-reborn.service
            # Remove last 2 lines from /etc/sudoers
            sudo sed -i '$d' /etc/sudoers
            sudo sed -i '$d' /etc/sudoers
            # Reload SystemD service daemon
            sudo systemctl daemon-reload
            # Notify the user that Anbox has been removed
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Done" "Anbox has successfully been removed." -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"
      else
            # Store value based on if snap is installed or not
            snapInstalled="true"

            echo $RUID
            echo $USER_HOME

            # Display notice to let the user know this could take a little bit
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Please be patient..." "This could take up to 5 minutes." -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"

            if [[ $(which snap | wc -l) -lt 1 ]]; then
                  echo "Snap is not installed"
                  snapInstalled="false"
            else
                  echo "Snap is already installed!"
            fi
            if [ -f /etc/pacman.d/reborn-mirrorlist ]; then
                  sudo pacman -S snapd lzip curl squashfs-tools wget tar unzip --noconfirm --needed
            else
                  sudo pacman -S snapd lzip curl squashfs-tools wget tar unzip --noconfirm --needed
            fi
            # If snap was not installed until the previous command...
            if [ "$snapInstalled" == "false" ]; then
                  sudo systemctl enable --now snapd.socket
                  sudo systemctl start snapd.socket
            fi

            # If Anbox is not already installed...
            if [[ $(snap list | grep "anbox" | wc -l) -lt 1 ]]; then
                  sudo snap install --devmode --beta anbox
            else
                  sudo snap refresh --beta --devmode anbox
            fi
            sudo modprobe ashmem_linux
            sudo modprobe binder_linux

            # Naviage to user's home directory
            cd $USER_HOME
            # Install the PlayStore and houdini for Anbox, allowing it to run ARM-only apps
            sudo -u ${RUID} wget https://raw.githubusercontent.com/geeks-r-us/anbox-playstore-installer/master/install-playstore.sh
            sudo -u ${RUID} chmod +x install-playstore.sh

            sudo bash ./install-playstore.sh
            sudo rm -f ./install-playstore.sh
            sudo rm -rf ./anbox-work
            # Add systemd service to start the Anbox Session Manager upon boot
            if [ ! -f /etc/systemd/system/anbox-reborn.service ]; then
                  # Create systemd file with the right permissions set for it
                  echo "[Unit]" >/etc/systemd/system/anbox-reborn.service
                  echo "Description=Start Anbox Session Manager at system boot." >>/etc/systemd/system/anbox-reborn.service
                  echo "After=snapd.socket" >>/etc/systemd/system/anbox-reborn.service
                  echo "" >>/etc/systemd/system/anbox-reborn.service
                  echo "[Service]" >>/etc/systemd/system/anbox-reborn.service
                  echo "Type=forking" >>/etc/systemd/system/anbox-reborn.service
                  echo "ExecStart=sudo snap start anbox" >>/etc/systemd/system/anbox-reborn.service
                  echo "" >>/etc/systemd/system/anbox-reborn.service
                  echo "[Install]" >>/etc/systemd/system/anbox-reborn.service
                  echo "WantedBy=multi-user.target" >>/etc/systemd/system/anbox-reborn.service
                  sudo chmod 644 /etc/systemd/system/anbox-reborn.service

                  # Enable the systemd service, reloading systemd as well
                  sudo systemctl daemon-reload
                  sudo systemctl enable anbox-reborn.service
                  # Allow this service to be run as root by modifying the sudoers file as securely as possible
                  echo "ALL ALL=(ALL) NOPASSWD: /usr/bin/systemctl start anbox-reborn.service" >> /etc/sudoers
                  echo "ALL ALL=(ALL) NOPASSWD: /usr/bin/systemctl stop anbox-reborn.service" >> /etc/sudoers
                  # Report status to the terminal
                  sudo systemctl status anbox-reborn.service
            fi
            # Display notice to let user know that Anbox is installed and ready to use
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Anbox is installed and ready to use" "The Playstore and ARM apps even work too!" -a "RebornOS FIRE" -u normal -i "/usr/share/icons/default/reborn-updates.svg"
      fi
}

function SetupWormhole()
{
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      if [[ $(which snap | wc -l) -lt 1 ]]; then
            echo "Snap is not installed"
            # Install snapd, double checking to make sure it is necessary
            sudo pacman -S snapd --needed --noconfirm
            # Start and enable the snapd services
            sudo systemctl enable --now snapd.socket
            sudo systemctl start snapd.socket
      fi
      # If wormhole is not already installed, install it. If it is, update it
      if [[ $(snap list | grep "wormhole" | wc -l) -lt 1 ]]; then
            sudo snap install wormhole
      else
            sudo snap refresh wormhole
      fi

      # Send the file via Wormhole
      SendWormhole &
      sudo -u ${RUID} wormhole send "$1" &> $USER_HOME/output.txt &
}

function SendWormhole()
{
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      # Reset value of isTrue to false
      isTrue="false"
      while [ "$isTrue" == "false" ]; do
            if [[ $(grep "wormhole receive" $USER_HOME/output.txt | wc -l) -gt 0 ]]; then
                  isTrue="true"
            fi
      done
      # Display the code to receive the file
      sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send "Available Now" "Code to recieve file: $(grep 'wormhole receive' $USER_HOME/output.txt | cut -d ' ' -f3)" -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"

      # Output the size and name of the file being sent
      sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send "Please keep RUM open until it has been recieved" -a "RebornOS FIRE" -u normal -i "/usr/share/icons/default/reborn-updates.svg"

      # Reset value of isTrue to false
      isTrue="false"
      while [ "$isTrue" == "false" ]; do
            if [[ $(grep "Confirmation received. Transfer complete." $USER_HOME/output.txt | wc -l) -gt 0 ]]; then
                  isTrue="true"
            fi
      done
      # Let the user know that the file has been received by the user
      sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send "Recipient received the file / folder." "Wormhole transfer complete." -a "RebornOS FIRE" -u normal -i "/usr/share/icons/default/reborn-updates.svg"
      # Remove output.txt
      sudo rm -f $USER_HOME/output.txt
}

function ReceiveWormhole()
{
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)
      # Move into the Downloads folder of the user
      cd $USER_HOME/Downloads
      # Receive the file using the code in $1
      sudo -u ${RUID} echo "y" | wormhole receive $1
      sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send "File / Folder Recieved" "It is now located in your Downloads folder" -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"
      # Beautify terminal output by ending with a new line.
      echo
}

BuildMycroft()
{
      # Detect the user-specified terminal
      terminal=$1
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      cd $USER_HOME
      if [ -d $USER_HOME/mycroft-core ]; then
            rm -rf mycroft-core
      fi
      sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" git clone https://github.com/MycroftAI/mycroft-core.git
      cd mycroft-core
      sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" eval $terminal bash $USER_HOME/mycroft-core/dev_setup.sh
}

StartMycroft()
{
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      if [ ! -d $USER_HOME/mycroft-core ]; then
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Mycroft is not installed" "Cannot run commands" -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"
      else
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" $USER_HOME/mycroft-core/start-mycroft.sh all &
            # Display an update for the user via a notification
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Mycroft is ready for you" -a "RebornOS FIRE" -u normal -i "/usr/share/icons/default/reborn-updates.svg"
      fi
}

StopMycroft()
{
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      if [ ! -d $USER_HOME/mycroft-core ]; then
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Mycroft is not installed" "Cannot run commands" -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"
      else
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" $USER_HOME/mycroft-core/stop-mycroft.sh
            # Display an update for the user via a notification
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Mycroft has been terminated" -a "RebornOS FIRE" -u normal -i "/usr/share/icons/default/reborn-updates.svg"
      fi
}

TextMycroft()
{
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      if [ ! -d $USER_HOME/mycroft-core ]; then
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Mycroft is not installed" "Cannot run commands" -a "RebornOS FIRE" -u critical -i "/usr/share/icons/default/reborn-updates.svg"
      else
            # Run command as the user running Mycroft, outputting everything to a text file so as to record the PID that is held therein
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" echo "$1" | $USER_HOME/mycroft-core/bin/mycroft-cli-client > $USER_HOME/mycroft.txt &

            while [[ $(grep "| INFO " $USER_HOME/mycroft.txt | grep "Connected" | wc -l) -eq 0 ]]; do
                  echo "Waiting for the connection to be made with Mycroft..."
            done

            MYCROFT_PID=$(grep "| INFO " $USER_HOME/mycroft.txt | grep "Connected" | awk '{print $8}')
            # Wait 7 seconds before killing the process
            sleep 7

            # Kill the process
            kill -SIGTERM $MYCROFT_PID

            # Display an update for the user via a notification
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" notify-send  "Mycroft is now built and ready to go!" -a "RebornOS FIRE" -u normal -i "/usr/share/icons/default/reborn-updates.svg"
      fi
}

RemoveMycroft()
{
      # Detect the user-specified terminal
      terminal=$1
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')
      # Translate Real Username to Real User ID
      # Used for setting dconf values, like this:
      # sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /some/value/goes/here true
      RUSER_UID=$(id -u ${RUID})
      USER_HOME=$(grep ${RUID} /etc/passwd | rev | cut -f2 -d : | rev)

      sudo rm /usr/share/applications/mycroft.desktop
      if [ -d $USER_HOME/mycroft-core ]; then
            sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" eval $terminal bash $USER_HOME/mycroft-core/dev_setup.sh --clean
            sudo rm -rf $USER_HOME/mycroft-core
      fi
}

CleaningOptions()
{
      # If $1 == 1
      if [[ $1 == "1" ]]; then
            yes | pacman -Scc
      fi
      # If $2 == 1
      if [[ $2 == "1" ]]; then
            journalctl --vacuum-time=3d && sync
      fi
      # If $3 == 1
      if [[ $3 == "1" ]]; then
            reflector --verbose -p https --sort rate --save /etc/pacman.d/mirrorlist
      fi
}

export -f LightDM SDDM TestLightdm SDDMdefault LightDMdefault Grub InstallTools DesktopEnvironment Rollback ResetRollback Downgrade Contribution InstallAnbox SetupWormhole SendWormhole ReceiveWormhole BuildMycroft StartMycroft StopMycroft TextMycroft RemoveMycroft CleaningOptions
export PATH=$PATH:/var/lib/snapd/snap/bin

"$@"
