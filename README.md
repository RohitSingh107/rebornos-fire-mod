# RebornOS FIRE (Features and Improvements in RebornOS made Easy)

===== Notes by Rafael =====

Clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/rebornos-fire-mod.git
```

How do we generate RebornOS FIRE once the repository is cloned?

RebornOS FIRE requieres the file **rebornos-fire.tar.gz**

If we have modified the existing repository, then must delete the file **rebornos-fire.tar.gz**

**NOTE**: Remember to change the version number of RebornOS FIRE.

How create the new file **rebornos-fire.tar.gz**?

Into rebornos-fire-mod directory, run from terminal:

```
tar -czv --exclude='PKGBUILD' --exclude='README.md' -f rebornos-fire.tar.gz *
```

Update the sums:

```
updpkgsums
```

Then compile:

```
makepkg
```

Ready!

===== End notes by Rafael =====

## Purpose
To make tasks that are commonly more advanced (need interaction with a terminal for) possible for a newbie who may not be comfortable in an Arch-based world at first.

### Functions

- Easily manage the most common display managers (LightDM and SDDM) which lack simple GUI management applications.
![](Screenshots/DisplayManagers.png)

- Install additional DEs if so desired.
![](Screenshots/DesktopEnvironments.png)

- Clean out your system using `pacman`-specific commands. For instance, cleaning the package cache and removing unneccesary (often orphaned) packages.
![](Screenshots/SystemMaintenance.png)
![](Screenshots/Repair.png)

- Rollback your entire system to a previous date.
![](Screenshots/Rollback.png)

- Expose the user to a few simple applications currently found in the AUR that can be used to make tasks easier in Arch Linux.
![](Screenshots/Addons.png)
![](Screenshots/Tools.png)

### Install

#### RebornOS
1) Open up your favorite Software Installer (most likely Pamac) and search for "reborn-updates", clicking `Install`. If you prefer a terminal, just enter the below text:
```
sudo pacman -S rebornos-fire --noconfirm
```
DONE!

#### Arch Linux (or any derivative)
1) Navigate to our Gitlab page here (which you are obviously already at since you are reading this) by the following URL: https://gitlab.com/reborn-os-team/reborn-updates-and-maintenance

2) Download the `rebornos-fire` package (the only file ending in .zst).

3) Install the downloaded file either by right-clicking on it and selecting to "Open With..." your favorite Software Installer, or just through the terminal using the below command:
```
sudo pacman -U ${PATH_TO_FILE}
```
4) A new version came out since you last did this? No problem! If you are using a non-RebornOS derivitive of Arch Linux (or Arch itself), RebornOS FIRE will automatically determine whether or not an update is available, and update for you when you next launch it if necessary.
